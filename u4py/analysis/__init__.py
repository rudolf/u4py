"""
Analysis module containing several submodules for data analysis.
"""

from . import classify, inversion, other, processing, spatial
from .classify import *
from .inversion import *
from .other import *
from .processing import *
from .spatial import *
