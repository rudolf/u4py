"""
Uses the shapes classified by the main workflow and generates a report for
each anomaly.
"""

import configparser
import datetime
import logging
import os
from pathlib import Path

import geopandas as gp
import numpy as np
from tqdm import tqdm

import u4py.analysis.processing as u4proc
import u4py.analysis.spatial as u4spatial
import u4py.io.docx_report as u4docx
import u4py.io.tex_report as u4tex
import u4py.plotting.plots as u4plots
import u4py.utils.cmd_args as u4args
import u4py.utils.projects as u4proj


def main():
    args = u4args.load()
    if args.input:
        proj_path = args.input
    else:
        # proj_path = r"~\Documents\ArcGIS\U4_projects\PostProcess_ClassifiedShapesHLNUG.u4project"
        proj_path = (
            "~/Documents/umwelt4/PostProcess_ClassifiedShapesHLNUG.u4project"
        )
        # proj_path = "~/Documents/umwelt4/PostProcess_ClassifiedShapes_onlyLarge.u4project"
        # proj_path = (
        #     "~/Documents/umwelt4/PostProcess_ClassifiedShapes_hazard.u4project"
        # )
    project = u4proj.get_project(
        proj_path=Path(proj_path).expanduser(),
        required=[
            "base_path",
            "places_path",
            "output_path",
            "results_path",
            "diff_plan_path",
        ],
        interactive=False,
    )

    if project.getboolean("config", "is_hlnug"):
        u4plots.IMAGE_FORMATS = ["png"]
        u4plots.IS_HLNUG = True

    # Setting up paths
    output_path = os.path.join(
        project["paths"]["output_path"],
        f"Detailed_Maps{project['metadata']['report_suffix']}",
    )
    os.makedirs(output_path, exist_ok=True)
    class_shp_fp = os.path.join(
        project["paths"]["results_path"], "Classified_Shapes.gpkg"
    )
    cls_shp_fp_filtered = os.path.join(
        project["paths"]["results_path"],
        f"Filtered_Classified_Shapes{project['metadata']['report_suffix']}.gpkg",
    )
    hlnug_path = os.path.join(
        project["paths"]["places_path"],
        "Classifier_shapes",
        "classifier_shapes.gpkg",
    )
    dem_path = os.path.join(
        os.path.split(project["paths"]["diff_plan_path"])[0],
        "DGM1_2021",
        "raster_2021",
    )
    if project.getboolean("config", "is_hlnug"):
        contour_path = class_shp_fp
    else:
        contour_path = os.path.join(
            project["paths"]["results_path"],
            "thresholded_contours_all_shapes.gpkg",
        )

    # Read Data
    if project.getboolean("config", "use_filtered"):
        if not os.path.exists(cls_shp_fp_filtered) or project.getboolean(
            "config", "overwrite_data"
        ):
            gdf_filtered = filter_shapes(
                class_shp_fp, cls_shp_fp_filtered, project
            )
            gdf_filtered = reverse_geolocate(
                gdf_filtered,
                project["paths"]["results_path"],
                cls_shp_fp_filtered,
            )
        else:
            gdf_filtered = gp.read_file(cls_shp_fp_filtered)
    else:
        if not os.path.exists(cls_shp_fp_filtered) or project.getboolean(
            "config", "overwrite_data"
        ):
            gdf_filtered = gp.read_file(class_shp_fp)
            gdf_filtered = reverse_geolocate(
                gdf_filtered,
                project["paths"]["results_path"],
                cls_shp_fp_filtered,
            )
        else:
            gdf_filtered = gp.read_file(cls_shp_fp_filtered)

    # Generating Plots
    if project.getboolean("config", "generate_plots"):
        # Assembling arguments for processing
        args = [
            (
                row,
                gdf_filtered.crs,
                output_path,
                hlnug_path,
                project,
                dem_path,
                contour_path,
                project.getboolean("config", "overwrite_plots"),
                project["metadata"]["report_suffix"],
            )
            for row in gdf_filtered.iterrows()
        ]
        # args = args[:20]
        if project.getboolean("config", "use_parallel"):
            u4proc.batch_mapping(args, wrap_map_worker, "Generating Plots")
        else:
            for arg in tqdm(
                args,
                total=len(args),
                desc="Generating Plots",
                leave=False,
            ):
                wrap_map_worker(arg)

    if project.getboolean("config", "is_hlnug"):
        hlnug_data = gp.read_file(
            os.path.join(
                project["paths"]["places_path"],
                "HLNUG Daten",
                "SHP",
                "RD_Rutschungen_gesamt.shp",
            )
        )
    else:
        hlnug_data = gp.GeoDataFrame()

    # Generating individual files and final report (for PDF).
    if project.getboolean("config", "generate_document"):
        if not project.getboolean("config", "is_hlnug"):
            for row in tqdm(
                gdf_filtered.iterrows(),
                desc="Generating tex files",
                total=len(gdf_filtered),
            ):
                u4tex.site_report(row, output_path, "tex_includes")
            if project.getboolean("config", "single_report"):
                u4tex.main_report(
                    output_path,
                    project["metadata"]["report_title"],
                    project["metadata"]["report_subtitle"],
                    project["metadata"]["report_suffix"],
                )
            else:
                u4tex.multi_report(output_path)
        else:
            for row in tqdm(
                gdf_filtered.iterrows(),
                desc="Generating docx files",
                total=len(gdf_filtered),
            ):
                u4docx.site_report(
                    row,
                    output_path,
                    "docx_reports" + project["metadata"]["report_suffix"],
                    hlnug_data,
                )


def filter_shapes(
    input_path: os.PathLike,
    output_path: os.PathLike,
    project: configparser.ConfigParser,
):
    """Filters the classified shapes by known geohazards

    :param input_path: The path to the file where all classified shapes are located.
    :type input_path: os.PathLike
    :param output_path: Path where to save the filtered shapes as shapefile.
    :type output_path: os.PathLike
    :param project: The project config for paths etc...
    :type project: configparser.ConfigParser
    """
    class_shp_gdf = gp.read_file(input_path)

    lands_f = class_shp_gdf.landslides_num_inside > 0
    karst_f = class_shp_gdf.karst_num_inside > 0
    rockf_f = class_shp_gdf.rockfall_num_inside > 0
    filter_all = np.logical_or(np.logical_or(lands_f, karst_f), rockf_f)
    gdf_filtered = class_shp_gdf[filter_all]
    return gdf_filtered


def reverse_geolocate(
    gdf_filtered: gp.GeoDataFrame,
    output_path: os.PathLike,
    cls_shp_fp_filtered: os.PathLike,
) -> gp.GeoDataFrame:
    # Do a reverse geocoding to get the address of the locations.
    cached_locations = os.path.join(output_path, "cached_locations.txt")
    if not os.path.exists(cached_locations):
        locations = u4spatial.reverse_geolocate(gdf_filtered)
        with open(cached_locations, "wt", encoding="utf-8") as cache:
            for loc in locations:
                cache.write(loc + "\n")
    else:
        with open(
            cached_locations, "rt", encoding="utf-8", newline="\n"
        ) as cache:
            locations = cache.readlines()
    if len(locations) != len(gdf_filtered):
        locations = u4spatial.reverse_geolocate(gdf_filtered)
        with open(cached_locations, "wt", encoding="utf-8") as cache:
            for loc in locations:
                cache.write(loc + "\n")
    gdf_filtered = gdf_filtered.assign(locations=locations)

    gdf_filtered.to_file(cls_shp_fp_filtered)
    gdf_filtered.to_crs("EPSG:4326").to_file(
        os.path.splitext(cls_shp_fp_filtered)[0] + ".geojson"
    )
    return gdf_filtered


def wrap_map_worker(args: tuple):
    """Parallel wrapper for `map_worker()`

    :param args: The function arguments.
    :type args: tuple
    """
    map_worker(*args)


def map_worker(
    row: tuple,
    crs: str,
    output_path: os.PathLike,
    hlnug_path: os.PathLike,
    project: configparser.ConfigParser,
    dem_path: os.PathLike,
    contour_path: os.PathLike,
    overwrite_plots: bool,
    suffix: str,
):
    """Calls the various plotting and reporting functions.

    :param row: The index and entry of a row in a GeoDataFrame of classified shapes.
    :type row: tuple
    :param crs: The crs of the dataset.
    :type crs: str
    :param output_path: The path where the output shall be saved.
    :type output_path: os.PathLike
    :param hlnug_path: The path where HLNUG data is found.
    :type hlnug_path: os.PathLike
    :param project: The project's config.
    :type project: configparser.ConfigParser
    :param dem_path: The path to the digital elevation model.
    :type dem_path: os.PathLike
    :param contour_path: The path to the contour file.
    :type contour_path: os.PathLike
    """

    shp_path = os.path.join(
        project["paths"]["places_path"],
        "Classifier_shapes",
        "Web_Queries" + suffix,
    )
    places_path = os.path.join(
        project["paths"]["places_path"], "OSM_shapes", "all_shapes.gpkg"
    )

    u4plots.detailed_map(
        row,
        crs,
        output_path,
        "known_features",
        hlnug_path,
        contour_path,
        plot_buffer=250,
        overwrite=overwrite_plots,
        places_path=places_path,
    )
    u4plots.satimg_map(
        row,
        crs,
        output_path,
        "known_features",
        contour_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )
    if not u4plots.IS_HLNUG:
        u4plots.diffplan_map(
            row,
            crs,
            output_path,
            "known_features",
            project["paths"]["diff_plan_path"],
            plot_buffer=100,
            overwrite=overwrite_plots,
        )
        u4plots.dem_map(
            row,
            crs,
            output_path,
            "known_features",
            dem_path,
            contour_path,
            plot_buffer=100,
            overwrite=overwrite_plots,
        )
    else:
        u4plots.dem_map(
            row,
            crs,
            output_path,
            "known_features",
            dem_path,
            contour_path,
            plot_buffer=100,
            overwrite=overwrite_plots,
            shp_path=shp_path,
        )
    u4plots.slope_map(
        row,
        crs,
        output_path,
        "known_features",
        dem_path,
        contour_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )
    u4plots.aspect_map(
        row,
        crs,
        output_path,
        "known_features",
        dem_path,
        contour_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )
    u4plots.aspect_slope_map(
        row,
        crs,
        output_path,
        "known_features",
        dem_path,
        contour_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )
    u4plots.geology_map(
        row,
        crs,
        output_path,
        "known_features",
        contour_path,
        os.path.join(
            project["paths"]["places_path"], "Geologie (Kartiereinheiten).pkl"
        ),
        shp_path=shp_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
        use_internal=False,
    )
    u4plots.hydrogeology_map(
        row,
        crs,
        output_path,
        "known_features",
        contour_path,
        os.path.join(
            project["paths"]["places_path"], "Hydrogeologische Einheiten.pkl"
        ),
        shp_path=shp_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )
    u4plots.topsoil_map(
        row,
        crs,
        output_path,
        "known_features",
        contour_path,
        os.path.join(project["paths"]["places_path"], "legend_BFD50.pkl"),
        shp_path=shp_path,
        plot_buffer=100,
        overwrite=overwrite_plots,
    )

    try:
        if row[1].timeseries_num_psi > 5:
            u4plots.timeseries_map(
                row,
                crs,
                output_path,
                "known_features",
                contour_path,
                os.path.join(project["paths"]["psi_path"]),
                plot_buffer=100,
                overwrite=overwrite_plots,
            )
    except TypeError:
        logging.info("No PSI features")


if __name__ == "__main__":
    tic = datetime.datetime.now()
    main()
    duration = datetime.datetime.now() - tic
    print(f"Runtime: {duration}")
