"""
Module containing functions for easier and more convenient plotting.
"""

from . import axes, formatting, plots, preparation
from .axes import *
from .formatting import *
from .plots import *
from .preparation import *
